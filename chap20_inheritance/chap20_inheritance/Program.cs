﻿using System;

namespace chap20_inheritance
{
	class Shape
	{
		public void setWidth(int w)
		{
			width = w; }
		public void setHeight(int h)
		{
			height = h; 
		}
		protected int width;
		protected int height;
	}
	class Rectangle: Shape
	{
		public int getArea()
		{
			return (width * height);
		}
	}
	class MainClass
	{
		public static void Main (string[] args)
		{
			Rectangle r = new Rectangle ();
			r.setWidth (5);
			r.setHeight (2);
			Console.WriteLine (r.getArea ());
		}
	}
}
