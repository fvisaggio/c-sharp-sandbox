﻿using System;

namespace chap_15_arrays
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			//double[] arr = new double[10];
			double[] arr = {3.123,4.15};
			for (int i = 0; i < arr.Length; i++) {
				Console.WriteLine (arr [i]);
			}
			int [] marks = new int[]  { 99,  98, 92, 97, 95};// create an initilize it at the same time
			int[] score = marks; // re assign
			foreach (int j in marks)
			{
				Console.WriteLine( "mark is "+j);
			}

			int[,] a = new int[5, 2] {{0,0}, {1,2}, {2,4}, {3,6}, {4,8} };
			Console.WriteLine (a+" value in 2 , 1  "+ a[2,1]);
			foo ();
			ArrayFeatures ();
		}
		public static void foo(){
			Console.WriteLine ("");
			Console.WriteLine ("");

			Console.WriteLine ("jagged arrays below");

			/* a jagged array of 5 array of integers*/
			int[][] a = new int[][]{new int[]{0,0},new int[]{1,2},
				new int[]{2,4},new int[]{ 3, 6 }, new int[]{ 4, 8 } };
			int i, j;
			/* output each array element's value */
			for (i = 0; i < 5; i++)
			{
				for (j = 0; j <2; j++)
				{
					Console.WriteLine("a[{0}][{1}] = {2}", i, j, a[i][j]);
				}
			}

			Console.WriteLine ("testing out the add elements method to see the sum");
			// take in a bunch odf params as an array
			Console.WriteLine (AddElements (55, 11, 2, 33));
		}

		public static int AddElements(params int[] arr)
		{
			int sum = 0;
			foreach (int i in arr)
			{
				sum += i;
			}
			return sum; 
		}

		public static void ArrayFeatures()
		{
			int[] list = { 34, 72, 13, 44, 25, 30, 10 };
			int[] temp = list;
			Console.Write("Original Array: ");
			foreach (int i in list)
			{
				Console.Write(i + " ");
			}
			Console.WriteLine();
			// reverse the array
			Array.Reverse(temp);
			Console.Write("Reversed Array: ");
			foreach (int i in temp)
			{
				Console.Write(i + " ");
			}
			Console.WriteLine();
			//sort the array
			Array.Sort(list);
			Console.Write("Sorted Array: ");
			foreach (int i in list)
			{
				Console.Write(i + " ");
			}
			Console.WriteLine();

			Console.Write("Sorted in desc order Array: ");
			Array.Sort(list);
			Array.Reverse(list);
			foreach (int i in list)
			{
				Console.Write(i + " ");
			}
			Console.WriteLine();
		}
	}
}
