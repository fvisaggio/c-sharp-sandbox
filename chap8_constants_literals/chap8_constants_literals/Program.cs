﻿using System;

namespace chap8_constants_literals
{
	class MainClass
	{
		public static void Main (string[] args)
		{

			// 0x hexadeicaml 0 for octral 
			int x = 0x76;
			Console.WriteLine (Convert.ToInt16(x));

			// char contstants \t new tab
			Console.WriteLine("Hello\tWorld\n\n");

			// string literal has double quotes 
			string x2 = "hello \\ govna";

			// constants 
			const double pi = 3.14;

//			% ++ --  operators != == >- etc and or and not 

			// & | ^ (exlusive or) ~ ones compliment (flips bits)
			// bitwise operators they do the comparison bit by bit. 

			int a = 60; // 0011 1100
			int c = ~a;// flip bits 
			Console.WriteLine (c); // we flip it 

			// << binary left shift and binary right shift 

			//A << 2 will give 240, which is 1111 0000

			// *= , += , /= aka x /= 4 eq to x = x/4;

//			sizeof  seels size of data in bytes
//			String xy="so";
			int xy;
			/* example of sizeof operator */
			Console.WriteLine("The size of int is {0}", sizeof(int));
			Console.WriteLine("The size of short is {0}", sizeof(short));
			Console.WriteLine("The tyoe of double is {0}", typeof(String));
		}
	}
}
