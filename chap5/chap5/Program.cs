﻿using System;

namespace chap5
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			//MainClass.valueTypes ();
			//MainClass.referenceTypes ();
//			MainClass.pointerTypes (); use pointers for performance. 
		}
		public static void referenceTypes()
		{
			// reference to be stored in 
			object obj;
			obj = 100; // boxing or putting value type to object
			// reference to where the obj is stored  
			// object dynamic string 
			Console.WriteLine (obj);
//			** converting an object to a value type from a reference type is unboxing


			//** dynamic types 
			// type checking at runtime
//			dynamic d = 20;
//			Console.WriteLine (d);
//			d = "foo";
//			Console.WriteLine (d);
		
			string x= @"foo \"; //literal strings  
			Console.WriteLine (x);
		}
			
		public static void valueTypes()
		{
			// value tyspes 
			//			inherit from System.ValueType
			// for money always use decimal ! 
			//double is faster , but decimal always adds up 
			decimal x = 10.00M;
			Console.WriteLine ("djdjd {0} is not {1}", x, 3);
			Console.WriteLine ("Hello World!");
			double x2 = Math.Pow (2, 8);
			Console.WriteLine ("Hello World!" + x2);
			//			byte  8 bits in a byte 2 ^8  256 so is 0-255 
			//			short uint  ulong 
			//			short 16 Bit double 64 Bit  decimal 128 


			bool m = false; 
			Console.WriteLine (m);

			Console.WriteLine("Size of int: {0}", sizeof(int));
			Console.WriteLine("Size of byte: {0}", sizeof(byte));

			Console.ReadLine();

		}
	}
}
