﻿using System;

namespace chap27_exception_handling
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			try{
				int i=0;
				i=9/i;
			}
			catch (DivideByZeroException e) {
				Console.WriteLine ("remember dont catch generic exceptions {0}", e);
			}
			finally {
				Console.WriteLine ("Hello World!");
			}
			Console.ReadLine ();
		}
	}
}
