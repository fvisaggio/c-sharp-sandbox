﻿using System;

namespace first
{
	class namespace_class
	{
		public static void print()
		{
			Console.WriteLine ("in the first namespace");
		}
	}
}
namespace second
{
	class namespace_class
	{
		public static void print()
		{
			Console.WriteLine ("we are in the 2nd namespace now");
		}
	}
}
class runner
{
	static void Main(string[] args)
	{
		// each namespace can define the same class with the same method of print
		first.namespace_class.print ();
		second.namespace_class.print ();
		Console.ReadKey();
	} 
}