﻿using System;
public class MyClass
{
	[Obsolete("Don't use OldMethod, use NewMethod instead", true)]
	// this attribute gives a compile time error saying to use the new method instead 
	static void OldMethod()
	{
		Console.WriteLine("It is the old method");
	}
	static void NewMethod()
	{
		Console.WriteLine("It is the new method");
	}
	public static void Main()
	{
		OldMethod();
	}
}