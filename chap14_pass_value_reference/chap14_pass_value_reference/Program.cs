﻿using System;

namespace chap14_pass_value_reference
{
	class MainClass
	{
		public static void swap(int x, int y){
			int temp;
			temp = x; 
			x = y;   
			y = temp;
		}
		public static void RefSwap(ref int x, ref int y){
			int temp;
			temp = x; 
			x = y;   
			y = temp;
		}
		public static void getValue(out int x)
		{
			int temp = 55;
			x = temp;
		}
		public static void AddTwo(ref int one, ref int two)
		{
			one += 2;
			two += 2;
		}
		public static void getValues(out int x, out int y )
		{
			Console.WriteLine("Enter the first value: ");
			x = Convert.ToInt32(Console.ReadLine());
			Console.WriteLine("Enter the second value: ");
			y = Convert.ToInt32(Console.ReadLine());
		}
		public static void Main(string []args)
		{
			// we can see you pass by value
			int j = 1;
			int p = 2;
			swap (j, p);
			Console.WriteLine ("they are still 1 and 2 as shown ... {0} {1}", j, p);
			RefSwap (ref j, ref p);
			Console.WriteLine ("now check out the ref keyword it wont be 1 and 2 .. {0} {1} ", j, p);

			// 
			int m = 66;
			getValue (out m);
			Console.WriteLine ("shouldnt be 66 it is actually"+m);

			int x = 1;
			int y = 1;
			AddTwo (ref x, ref y);
			Console.WriteLine ("we just called the add two to 1 and 1  .. {0} {1} ", x, y);

			int uu; int jj;
			getValues (out uu, out jj);
			Console.WriteLine (" {0} {1} ", uu, jj);

		}
	}
}
