﻿using System;

namespace chap_12_public
{
	class Rectangle
	{
		public double length;
		public double width;

		public double getArea()
		{
			return length * width;
		}

		public void Display()
		{
			Console.WriteLine("Length: {0}", length);
			Console.WriteLine("Width: {0}", width);
			Console.WriteLine("Area: {0}", getArea());
		}
	}// end rect
	class ExecuteRectangle
	{
		static void Main(string[] args)
		{
			Rectangle r = new Rectangle();
			r.length = 4.5;
			r.width = 3.5;
			r.Display();
			Console.ReadLine();
		}
	}

}
