﻿using System;

namespace chap17_structs
{
	struct Books
	{
		private string title;
		private string author;
		private string subject;
		private int book_id;
		public void getValues(string t, string a, string s, int id)
		{
			title = t;
			author = a;
			subject = s;
			book_id = id;
		}
		public void display()
		{
			Console.WriteLine("Title : {0}", title);
			Console.WriteLine("Author : {0}", author);
			Console.WriteLine("Subject : {0}", subject);
			Console.WriteLine("Book_id :{0}", book_id);
		}
	};
	class MainClass
	{
		public static void Main (string[] args)
		{
			Console.WriteLine ("Hello World!");

			Books Book1 = new Books ();
			Book1.getValues ("harry potter", "john doe", "wizard",6721);
			Book1.display ();
		}
	}
}
