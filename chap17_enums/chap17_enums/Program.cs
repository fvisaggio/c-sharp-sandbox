﻿using System;

namespace chap17_enums
{
	class MainClass
	{
		enum Engine { i4,i6,v6,v8,v10 };

		public static void Main (string[] args)
		{
			int smallest = (int) Engine.i4;
			int largest = (int) Engine.v10;
			Console.WriteLine(smallest);
			Console.WriteLine(largest);

		}
	}
}
