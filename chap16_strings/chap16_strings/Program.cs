﻿using System;

namespace chap16_strings
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			// strings concat with the +

			char[] letters = { 'f', 'o', 'o' };
			string foo = new string (letters);
			string x = "yiou";
			Console.WriteLine (foo + "   = "+x);
			string[] sarray = { "hello" + "this" + "is" + "dog" };
			string message = String.Join ("  ", sarray);
			Console.WriteLine (message); 
			string str = "a man a plan a canal panamam";
			Console.WriteLine (str.Substring(10));
		}
	}
}
