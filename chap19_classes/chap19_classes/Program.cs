﻿using System;

namespace chap19_classes
{
	class MainClass
	{
		class Box {

			private double length;
			private double breadth;  // Breadth of a box
			private double height;   // Height of a box
			public static int num;
			private void count()
			{
				num++; }
			public int getCount()
			{
				return num; 
			}
			~Box() // this is our destructor 
			{
				Console.WriteLine(this.GetHashCode()+" Object being deleted");
				Console.WriteLine("boxes created so fa"+getCount());
			}
			public Box ()
			{
				count();
				Console.WriteLine("098908908908");
			}
			public Box (string ei)
			{
				count();
				Console.WriteLine("OVERLOAAADDD constructor");
			}
			public void setLength( double len )
			{
				length = len;
			}
			public void setBreadth( double bre )
			{
				breadth = bre;
			}
			public void setHeight( double hei )
			{
				height = hei;
			}
			public double getVolume()
			{
				return length * breadth * height;
			}
			public static void Silly()
			{
				Console.WriteLine (" oh so silly");
			}
		}

		public static void Main (string[] args)
		{
			Box Box1 = new Box();
			Box Box12 = new Box("23");

			double volume;
			// Declare box of type Box
			// box 1 specification
			Box1.setLength(6.0);
			Box1.setBreadth(7.0);
			Box1.setHeight(5.0);
		
			volume = Box1.getVolume ();
			Console.WriteLine("Volume of Box1 : {0}" ,volume);
			Box.Silly ();
		}
	}
}
