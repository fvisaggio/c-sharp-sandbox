﻿using System;

namespace OperatorOvlApplication
{
	class Box {
		private double length;
		// Length of a box
		private double breadth;     // Breadth of a box
		private double height;      // Height of a box
		public Box (int len=0, int bre=0, int hei=0)
		{
			length=len;
			breadth=bre;
			height=hei;
		}
		public double getVolume()
		{
			return length * breadth * height;
		}
		public void setLength( double len )
		{
			length = len;
		}
		public void setBreadth( double bre )
		{
			breadth = bre;
		}
		public void setHeight( double hei )
		{
			height = hei;
		}
		 //Overload + operator to add two Box objects.
		// without that you will get an error because theres no default + operator on this obj 
		public static Box operator+ (Box b, Box c)
		{
			Box box = new Box();
			box.length = b.length + c.length;
			box.breadth = b.breadth + c.breadth;
			box.height = b.height + c.height;
			return box;
		} 
		public static Box operator! (Box m)
		{
			Box box = new Box (0, 0, 0);
			return box;
		}

	}
	class Tester
	{
		static void Main(string[] args)
		{
			Box Box1 = new Box();
			Box Box2 = new Box();
			Box Box3 = new Box();
			double volume = 0.0;
			// box 1 specification
			Box1.setLength(6.0);
			Box1.setBreadth(7.0);
			Box1.setHeight(5.0);
			// box 2 specification
			Box2.setLength(12.0);
			Box2.setBreadth(13.0);
			Box2.setHeight(10.0);
			// volume of box 1
			// Declare Box1 of type Box
			// Declare Box2 of type Box
			// Declare Box3 of type Box
			// Store the volume of a box here
			volume = Box1.getVolume();
			Console.WriteLine("Volume of Box1 : {0}", volume);
			// volume of box 2
			volume = Box2.getVolume();
			Console.WriteLine("Volume of Box2 : {0}", volume);
			// Add two object as follows:
			Box3 = Box1 + Box2;
			// volume of box 3
			volume = Box3.getVolume();
			Console.WriteLine("Volume of Box3 : {0}", volume);

			Box Box4 = new Box();
			Box4 = !Box2;
			volume=  Box4.getVolume();
			Console.WriteLine("Volume of Box4 : {0}", volume);

			Console.ReadKey();
		} }
}
