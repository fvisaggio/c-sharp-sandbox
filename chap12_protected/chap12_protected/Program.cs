﻿using System;
namespace RectangleApplication
{
	class Rectangle
	{
		//member variables
		internal double length;
		private double width;
		public void Acceptdetails()
		{
			Console.WriteLine ("enter length");
			length = Convert.ToDouble(Console.ReadLine ());
			Console.WriteLine ("enter width");
			width = Convert.ToDouble(Console.ReadLine ());
		}
		public double GetArea()
		{
			return length * width;
		}
		public void Display()
		{
			Console.WriteLine("Length: {0}", length);
			Console.WriteLine("Width: {0}", width);
			Console.WriteLine("Area: {0}", GetArea());
		}
		public int factorial(int num)// recursion just for kicks 
		{
			int output;
			if (num == 1) {
				return 1;
			}
			else {
				output = num * factorial(num - 1);
				return output;
			}
		}
	}//end class Rectangle
	class ExecuteRectangle
	{
		static void Main(string[] args)
		{
			Rectangle r = new Rectangle();
			r.length = 5; // this is allowed because thats internal and we are in the same file
			r.Acceptdetails();
			r.Display();
			Console.WriteLine (r.factorial (6));
			Console.ReadLine();
		} }
}