﻿using System;
using System.Text.RegularExpressions;

namespace nd_demo
{	
	class MainClass
	{
		private static void showMatch(string text, string expression)
		{
			Console.WriteLine("The Expression: " + expression);
			MatchCollection mc = Regex.Matches(text, expression);
			foreach (Match m in mc)
			{
				Console.WriteLine(m);
			}
		}
		public static void Main (string[] args)
		{
			string str = "make maze and manage to measure it";
			Console.WriteLine("Matching words start with 'm' and ends with 'e' so no mickey:");
			showMatch(str, @"\bm\S*e\b");
			Console.ReadKey();
		}
	}
}
