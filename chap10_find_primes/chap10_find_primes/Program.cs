﻿using System;

namespace chap10_find_primes
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Console.WriteLine ("Find all primes to 100");

			for (int currentNumber = 1; currentNumber <= 100; currentNumber++) {
				bool isPrime = true;
				for (int possibleFactor = 2; possibleFactor <= (currentNumber / possibleFactor); possibleFactor++) {
					if (currentNumber % possibleFactor == 0) {
						isPrime = false;
						break;
					}
				}
				if (isPrime) {
					Console.WriteLine (currentNumber);
				}
			}
			Console.WriteLine ("testing break below");
		
			Console.WriteLine ("");
			int a = 10;
			while (a < 20) {
				Console.WriteLine ("A is {0}", a);
				a++;
				if (a > 15) {
					break;
				}
			}
			Console.WriteLine ("testing continue below");

			Console.WriteLine ("");
			a = 10;

			do{
				if (a == 15) {
					a++;
					continue;
				}
				Console.WriteLine ("A is {0}", a);
				a++;
			}while (a<20);
		}
	}
}
