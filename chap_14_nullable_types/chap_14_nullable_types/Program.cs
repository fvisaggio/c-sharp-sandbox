﻿using System;

namespace chap_14_nullable_types
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			int? num1 = null;
			int? num2 = 45;
			double? num3 = null;
			double? num4 = 3.14157;
			bool? boolval = new bool?();
			// display the values
			Console.WriteLine("Nullables at Show: {0}, {1}, {2}, {3}",
				num1, num2, num3, num4);
			Console.WriteLine("A Nullable boolean value: {0}", boolval);

			// null coalescing opp 
			double? num11 = null;
			double? num22 = 3.14157;
			double num33;
			num33 = num11 ?? 5.34;  
			Console.WriteLine(" Value of num3: {0}", num33);
			num33 = num22 ?? 5.34;
			Console.WriteLine(" Value of num3: {0}", num33);

		}
	}
}
