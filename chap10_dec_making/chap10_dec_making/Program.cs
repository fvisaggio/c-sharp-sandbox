﻿using System;

namespace chap10_dec_making
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			int x = 3; 
			if (x == 1) {
				Console.WriteLine ("Hello World!");
			} else if (x == 2) {
				Console.WriteLine ("222");

			} else {
				Console.WriteLine ("c# is nice");
			}
		
			switch (x) {
			case 1:
				// do nothing 
				break;
			case 2:
				Console.WriteLine ("we fall through because no break here!");
				break;
			case 4:
				Console.WriteLine ("22222");
				break;
			default:
				Console.WriteLine ("default");
				break;
			}


			// below is their new style if 

//			Expr1 ? Exp2: Exp3;   if its true do 2 otherwise 3
			Console.WriteLine (x);

			for (int i = 0; i < 10; i++)
			{
				if (i == 5) {
					continue;
				}
				Console.WriteLine(i);
			}

			int xx=0;
			while (xx<9)
			{
				Console.WriteLine(xx);
				xx++;
			}

			do {
				Console.WriteLine ("never was a huge fan of the do while");
				xx--;
			} while(xx > 5);

		}
	}
}
