﻿using System;

namespace chap6_type_conversion
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			double d = 5673.74;
			int i;
			// cast double to int.
			i = (int)d;
			int j = 8;
			double m = j;
			Console.WriteLine(i);
			Console.WriteLine(m);


			// c sharp has conversion methods ToInt16 ToString etc
			float f = 53.005f;

			bool b = true;
			Console.WriteLine(i.ToString());
			Console.WriteLine(f.ToString());
			Console.WriteLine(d.ToString());
			Console.WriteLine(b.ToString());

			double pi = 3.1456;

			Console.WriteLine ("enter decimal");
			double num=0;
			string foo;
			num = Convert.ToDouble(Console.ReadLine());

			Console.WriteLine (num);



		}			

	}
}
