﻿using System;
using System.IO;

namespace binaryFileIO
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			BinaryWriter bw;
			int i = 4;
			double d = 3.14;
			string s = "dot net";
			bool b = true;

			try{
				bw = new BinaryWriter(new FileStream("testdata",FileMode.Create));

			}
			catch (IOException e) {
				Console.WriteLine (e.Message + " file can not be created properly.");
				return;
			}
			try{
				bw.Write(i);
				bw.Write(d);
				bw.Write(s);
				bw.Write(b);
			}
			catch (IOException e) {
				Console.WriteLine (e.Message + "\n was not able to write to the file");
				return;
			}
			bw.Close ();

			//read file
			try {
				using (BinaryReader br = new BinaryReader (new FileStream ("testdata",FileMode.Open)))
				{
					
					i = br.ReadInt32();
					Console.WriteLine("Integer data: {0}", i);
					d = br.ReadDouble();
					Console.WriteLine("Double data: {0}", d);
					s = br.ReadString();
					Console.WriteLine("String data: {0}", s);
					b = br.ReadBoolean();
					Console.WriteLine("Boolean data: {0}", b);
				}	
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message + "catching non specific exceptions is bad");
			}
			Console.ReadLine ();
		}
	}
}
