﻿using System;
using System.IO;
namespace FileApplication
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				//FileStream io = new FileStream ("blah.txt", FileMode.Open, FileAccess.Read, FileShare.Read);
				// you would then have to close that too

				// Create an instance of StreamReader to read from a file.
				// The using statement also closes the StreamReader otherwise one could do a sr.Close()
				using (StreamReader sr = new StreamReader("../../foo.txt"))
				{
					string line;
					// Read and display lines from the file until
					// the end of the file is reached.
					while ((line = sr.ReadLine()) != null)
					{
						Console.WriteLine(line);
					}
				}
			}
			catch (Exception e)
			{
				// Let the user know what went wrong.
				Console.WriteLine("The file could not be read:");
				Console.WriteLine(e.Message);
			}
			Console.ReadKey();
			// you can also mess with Direcotires using DirectoryInfo
			// theres also a FileInfo Class that tell you things the windows file system knows about the file
			// ie the extension type lastAccessTime lastWriteTime
		}
	}
}