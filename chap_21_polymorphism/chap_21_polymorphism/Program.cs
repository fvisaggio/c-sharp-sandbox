﻿using System;

namespace chap_21_polymorphism
{
	class Printdata
	{
		public void print(int i)
		{
			Console.WriteLine("Printing int: {0}", i );
		}
		public void print(double f)
		{
			Console.WriteLine("Printing float: {0}" , f);
		}
		public void print(string s)
		{
			Console.WriteLine("Printing string: {0}", s);
		}
	}
	class MainClass
	{
		public static void Main (string[] args)
		{
				Printdata p = new Printdata();
				// Call print to print integer
				p.print(5);
				// Call print to print float
				p.print(500.263);
				// Call print to print string
				p.print("Hello C++");
		}
	}
}
